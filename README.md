### Requirements

Must have installed poetry as a global module

1. `cd app`
2. `poetry run python main.py data/patents.csv`
3. a new .csv file will be created inside the `/app/data` folder
