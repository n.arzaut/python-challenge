import csv
from fuzzywuzzy import fuzz
import sys


class Patent:
    def __init__(self, patent_id, organization, city, country, normalizedOrganization = None):
        self.patent_id = patent_id
        self.organization = organization
        self.normalizedOrganization = normalizedOrganization
        self.city = city
        self.country = country
  
    def __str__(self):
        return f'{self.organization}, {self.city}, {self.country}, {self.normalizedOrganization}'

# This function receives the csv file path and returns a list of Patent objects
def parse_csv_to_patents(file_path):
    data_list = []
    with open(file_path, 'r', encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            patent = Patent(row["patent_id"],row["organization"], row["city"], row["country"])
            data_list.append(patent)

    return data_list

def export_normalized_patents_to_csv(file_path, normalized_data):
    with open(file_path, 'w', encoding='utf-8', newline='') as csv_file:
        csv_writer = csv.DictWriter(csv_file, fieldnames=["patent_id","organization", "city", "country", "normalizedOrganization"])
        csv_writer.writeheader()
        for data in normalized_data:
            csv_writer.writerow(data.__dict__)



# This function recives the original data and returns a dictionary with the unique names of the organization with their amount of occurrances
def get_unique_occurances(objects: list[Patent]) -> dict:
    unique_patents = {}
    unique_latest_key = 0
    for current_object in objects:
        matching_patent_data = None
        matching_key = None
      
        for key, patent_data in unique_patents.items():
            # Checks for organization match
            if (
                patent_data["organization"] == current_object.organization
            ):
                matching_patent_data = patent_data
                matching_key = key


        if (matching_key):
            # If matches, adds an occurrance to the counter
            unique_patents[matching_key] =  {
                "count": matching_patent_data["count"] + 1,
                "organization": matching_patent_data["organization"],
                "country": matching_patent_data["country"],
                "city": matching_patent_data["city"]
            }
        else:
            # Else creates a new key:value entry
            unique_patents[unique_latest_key] = {
                "count": 1,
                "organization": current_object.organization,
                "country": current_object.country,
                "city": current_object.city
            }
            unique_latest_key += 1
        
        
    # Sorts the patents dict by their occurrances 
    sorted_patents = dict(sorted(unique_patents.items(), key=lambda x:x[1]["count"], reverse=True))
    return sorted_patents

# This function receives the unique organizations and their occurrances, and returns a dictionary with the canonical organizations
def get_canonical_organizations(unique_patents: dict) -> dict:
    canonical_patents = {}
    for patent_key, patent_data in unique_patents.items():
        match_data = None
        for _canonical_key, canonical_data in canonical_patents.items():
            if (
                fuzz.ratio(canonical_data["organization"], patent_data["organization"]) > 70
                and fuzz.ratio(patent_data["city"], canonical_data["city"]) > 70
                and patent_data["country"] == canonical_data["country"]
            ):
                match_data = canonical_data

        if (match_data == None):
            patent_data["organization"] = patent_data["organization"].split(', ')[0]
            canonical_patents[patent_key] = patent_data
    return canonical_patents

# This function receives the original data and the canonical organizations, and returns a list of normalized Patent objects
def normalize_patents(original_data, canonical_data):
    normalized_data = []

    for patent in original_data:
        best_match_ratio = 0
        best_canonical_org = None
        # For each patent we search the best canonical organization
        for canonical_key, canonical_item in canonical_data.items():
            ratio = fuzz.ratio(patent.organization, canonical_item["organization"])
            if ratio > best_match_ratio:
                best_match_ratio = ratio
                best_canonical_org = canonical_item["organization"]

        normalized_patent = Patent(
            patent_id=patent.patent_id,
            organization=patent.organization,
            city=patent.city,
            country=patent.country,
            normalizedOrganization=best_canonical_org
        )
        normalized_data.append(normalized_patent)

    return normalized_data

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python script.py <csv_file_path>")
        sys.exit(1)

    csv_file_path = sys.argv[1]
    patents_data = parse_csv_to_patents(csv_file_path)
    unique_occurrances = get_unique_occurances(patents_data)
    canonical_patents = get_canonical_organizations(unique_occurrances)
    normalized_patents = normalize_patents(patents_data, canonical_patents)
    export_normalized_patents_to_csv('data/normalized_patents.csv', normalized_patents)
   
